<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Medicaid_programs</name>
   <tag></tag>
   <elementGuidId>955a3f6d-873d-49dc-a569-30ba617db9b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#radio_program_medicaid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='radio_program_medicaid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d33665da-f87b-4c31-96f8-8f6e3b7bbd39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>6c7a207e-eadc-4077-9882-b4c45d775881</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>programs</value>
      <webElementGuid>8ad5001d-4cb1-4d6d-a05d-29f915216cd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>radio_program_medicaid</value>
      <webElementGuid>fcfc06cd-4609-48d0-8695-66d7685e2cf1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Medicaid</value>
      <webElementGuid>e1942139-55f9-4349-abd6-5be2ab9d44a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;radio_program_medicaid&quot;)</value>
      <webElementGuid>7a41b112-93e2-4529-8995-c71f1a105810</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='radio_program_medicaid']</value>
      <webElementGuid>93283bdc-3e70-4103-ab62-571afbe3c3e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[3]/div/label[2]/input</value>
      <webElementGuid>cf868e31-981b-43d6-a150-30b2be4a316f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label[2]/input</value>
      <webElementGuid>8bf07db7-966c-43d2-9faa-eb55293fb231</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'programs' and @id = 'radio_program_medicaid']</value>
      <webElementGuid>d937df04-2768-4845-8cf2-d7a9f4d7369b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
