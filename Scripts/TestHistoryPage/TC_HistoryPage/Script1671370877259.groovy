import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.callTestCase(findTestCase('Function/LoginFunction'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Function/AppointmentFunction'), [('username') : username, ('password') : password, ('facility') : facility
        , ('readmission') : readmission, ('program') : program, ('visit_date') : visit_date, ('comment') : comment, ('status') : visit_date], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl('https://katalon-demo-cura.herokuapp.com/history.php#history')

WebUI.verifyElementText(findTestObject('HistoryPage Asset/Page_CURA Healthcare Service/historyDate'), visit_date)

WebUI.verifyElementText(findTestObject('HistoryPage Asset/Page_CURA Healthcare Service/historyFacility'), facility)

WebUI.verifyElementText(findTestObject('HistoryPage Asset/Page_CURA Healthcare Service/historyProgram'), program)

WebUI.verifyElementText(findTestObject('HistoryPage Asset/Page_CURA Healthcare Service/historyReadmission'), readmission)

WebUI.verifyElementText(findTestObject('HistoryPage Asset/Page_CURA Healthcare Service/historyComment'), comment)

WebUI.closeBrowser()

