import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl('https://katalon-demo-cura.herokuapp.com/#appointment')

WebUI.selectOptionByValue(findTestObject('AppointmentAsset/select_facility'), facility, true)

if (readmission == 'Yes') {
    WebUI.click(findTestObject('Object Repository/AppointmentAsset/Page_CURA Healthcare Service/label_Apply for hospital readmission'))
}

if (program == 'Medicare') {
    WebUI.click(findTestObject('Object Repository/AppointmentAsset/Page_CURA Healthcare Service/label_Medicare'))
} else if (program == 'Medicaid') {
    WebUI.click(findTestObject('Object Repository/AppointmentAsset/Page_CURA Healthcare Service/label_Medicaid'))
} else {
    WebUI.click(findTestObject('Object Repository/AppointmentAsset/Page_CURA Healthcare Service/input_None_programs'))
}

WebUI.setText(findTestObject('Object Repository/AppointmentAsset/Page_CURA Healthcare Service/input_Visit Date (Required)_visit_date'), 
    visit_date)

WebUI.setText(findTestObject('Object Repository/AppointmentAsset/Page_CURA Healthcare Service/textarea_Comment_comment'), 
    comment)

WebUI.click(findTestObject('Object Repository/AppointmentAsset/Page_CURA Healthcare Service/button_Book Appointment'))

WebUI.delay(5)

