<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SuiteAppointment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>a802b00d-708e-4171-bc3f-793f8c42cf3b</testSuiteGuid>
   <testCaseLink>
      <guid>e1eb21d4-6faa-4719-b020-d7dd1b9f335d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestMakeAppointment/TC_Appointment</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>660ce957-2754-420c-93ab-443a252b83fa</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataAppointment</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>660ce957-2754-420c-93ab-443a252b83fa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>facility</value>
         <variableId>a0155840-a76e-46bf-b3aa-f1eaf5e7bede</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>660ce957-2754-420c-93ab-443a252b83fa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>readmission</value>
         <variableId>b2544b9e-d823-4ace-ae65-d9b6059914d7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>660ce957-2754-420c-93ab-443a252b83fa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>program</value>
         <variableId>09f4e08f-8ea0-4a98-a39d-d4e6e2f1e66e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>660ce957-2754-420c-93ab-443a252b83fa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>visit_date</value>
         <variableId>56b6b00b-0770-47de-ac67-af82dc716ab7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>660ce957-2754-420c-93ab-443a252b83fa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>comment</value>
         <variableId>e276ffdb-54ac-4d68-ae16-4cf377dd1e56</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>660ce957-2754-420c-93ab-443a252b83fa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>5fafd350-7b35-4615-a772-33bc226b517f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>660ce957-2754-420c-93ab-443a252b83fa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>985d8727-484b-456d-adb4-38755549331e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>660ce957-2754-420c-93ab-443a252b83fa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>status</value>
         <variableId>dbed0b1a-49e0-43c9-a8da-415bbbef86c9</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
