<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>historyFacility</name>
   <tag></tag>
   <elementGuidId>70664a41-46e9-41df-be66-c3350331fb28</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-sm-10</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='history']/div/div[2]/div/div/div[2]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d937b52f-8cf2-4bc5-9e37-4891443857f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-10</value>
      <webElementGuid>115d22d7-0af7-4c35-9416-94ee42c0792e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Tokyo CURA Healthcare Center
                        </value>
      <webElementGuid>c11684b5-5665-4f92-a85e-0855408d58c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;history&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-offset-2 col-sm-8&quot;]/div[@class=&quot;panel panel-info&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;col-sm-10&quot;]</value>
      <webElementGuid>7b496470-1b49-4bb6-9bb5-994f7afa463b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='history']/div/div[2]/div/div/div[2]/div[2]</value>
      <webElementGuid>c1744a55-aab0-485e-9f3f-3cae994d769f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Facility'])[1]/following::div[1]</value>
      <webElementGuid>392ac6b1-a156-447c-a247-8a06b42946c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='History'])[2]/following::div[7]</value>
      <webElementGuid>06c6000c-2b7a-427d-b414-69544fb9296b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply for hospital readmission'])[1]/preceding::div[2]</value>
      <webElementGuid>395b3097-95cd-4f0f-b72d-bc712ef21e59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Healthcare Program'])[1]/preceding::div[5]</value>
      <webElementGuid>48f932bc-9951-4277-961c-da77390eccf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]</value>
      <webElementGuid>610eb288-09eb-4ce2-8e0b-18863cb779e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            Tokyo CURA Healthcare Center
                        ' or . = '
                            Tokyo CURA Healthcare Center
                        ')]</value>
      <webElementGuid>9d81d129-3598-4e33-9fb8-d1f918c9e4f8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
